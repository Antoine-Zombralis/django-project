"""TrueDjangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from TrueDjangoProject import settings
from app.views.image.add import ImageAddView
from app.views.image.delete import ImageDeleteView
from app.views.image.list import ImageListView
from app.views.image.update import ImageUpdateView
from app.views.index import IndexView
from app.views.registration.signup import signup

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='app_index'),
    path('image_add',
         ImageAddView.form_valid, name='app_image_add'),
    path('accounts/',
         include('django.contrib.auth.urls')),
    path('signup/',
         signup, name='signup'),
    path('user_list_images/<int:pk>',
         ImageListView.as_view(), name='app_current_user_list_images'),
    path('user_list_images/delete/<int:pk>',
         ImageDeleteView.as_view(), name='delete_image'),
    path('user_list_images/update/<int:pk>',
         ImageUpdateView.as_view(), name='update_image'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
