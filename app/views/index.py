from django.views.generic import ListView

from app.models.image import Image


class IndexView(ListView):
    template_name = 'index.html'
    model = Image

    def get_queryset(self):
        return Image.objects.all()
