from django.urls import reverse_lazy
from django.views.generic import UpdateView

from app.models.image import Image


class ImageUpdateView(UpdateView):
    model = Image
    fields = ['name']
    template_name = 'update_image.html'
    success_url = reverse_lazy('app_index')

