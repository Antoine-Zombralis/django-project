from django.views.generic import ListView

from app.models.image import Image


class ImageListView(ListView):
    template_name = 'current_user_images.html'
    model = Image

    def get_queryset(self):
        return Image.objects.all()

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Toutes vos images !'
        result['images'] = Image.objects.filter(creator=self.kwargs.get("pk"))
        return result


