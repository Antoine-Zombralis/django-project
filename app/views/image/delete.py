from django.urls import reverse, reverse_lazy
from django.views.generic import DeleteView

from app.models.image import Image


class ImageDeleteView(DeleteView):
    template_name = 'delete_image.html'
    model = Image
    success_url = reverse_lazy('app_index')

    def get_success_url(self):
        return reverse('app_index')

    def get_queryset(self):
        return Image.objects.all()

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['images'] = Image.objects.filter(pk=self.kwargs.get("pk"))
        return result



