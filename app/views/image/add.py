from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import CreateView

from app.forms.image import ImageForm
from app.models.image import Image


class ImageAddView(CreateView):
    template_name = 'image_add.html'
    model = Image
    form_class = ImageForm

    def get_success_url(self):
        return reverse('app_index')

    def form_valid(request):
        if request.method == "POST":
            form = ImageForm(request.POST, request.FILES)
            candidate = form.save(commit=False)
            candidate.creator = request.user
            candidate.save()
            return redirect('app_index')
        else:
            form = ImageForm
            return render(request, 'image_add.html', {'form': form})
