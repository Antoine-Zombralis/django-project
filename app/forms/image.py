from django.forms import models

from app.models.image import Image


class ImageForm(models.ModelForm):

    class Meta:
        model = Image
        fields = ['name', 'image_Main_Img']
