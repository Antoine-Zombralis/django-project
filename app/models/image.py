from django.contrib.auth.models import User
from django.db import models


class Image(models.Model):
    name = models.CharField(max_length=50)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    image_Main_Img = models.FileField(upload_to='images/',)
